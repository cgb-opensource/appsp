package com.anji.plus.ajpushlibrary.service;

import com.anji.plus.ajpushlibrary.model.AppSpModel;

/**
 * Copyright © 2018 anji-plus
 * 安吉加加信息技术有限公司
 * http://www.anji-plus.com
 * All rights reserved.
 * <p>
 * <p>
 * 对外的接口，方便集成
 * </p>
 */
public interface IAppSpCallback {

    void pushInfo(AppSpModel<String> appSpModel);

    /**
     * 请求错误，未能获取后端数据
     *
     * @param code 状态码
     * @param msg  错误信息
     */
    void error(String code, String msg);
}
