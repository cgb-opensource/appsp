package com.anji.sp.push.service;

import com.anji.sp.model.ResponseModel;
import com.anji.sp.push.model.po.PushMessagePO;
import com.anji.sp.push.model.vo.PushMessageVO;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务推送id记录 服务类
 * </p>
 *
 * @author kean
 * @since 2021-01-20
 */
public interface PushMessageService extends IService<PushMessagePO> {

    /** 创建
     * @param vo
     * @return
     */
    ResponseModel create(PushMessageVO vo);

    /** 根据id修改
     * @param vo
     * @return
     */
    ResponseModel updateById(PushMessageVO vo);

    /** 根据msgId修改
     * @param vo
     * @return
     */
    ResponseModel updateByMsgId(PushMessageVO vo);
    /** 根据id删除
     * @param vo
     * @return
     */
    ResponseModel deleteById(PushMessageVO vo);

    /** 根据id查询一条记录
     * @param vo
     * @return
     */
    ResponseModel queryById(PushMessageVO vo);

    /** 根据参数分页查询列表
     * @param vo
     * @return
     */
    ResponseModel queryByPage(PushMessageVO vo);

    /** 根据appKey 和msgId查询消息历史
     * @param vo
     * @return
     */
    ResponseModel queryByMsgId(PushMessageVO vo);


    /** 根据appkey和msgId查询推送历史
     * @param vo
     * @return
     */
    ResponseModel queryAmountByMsgId(PushMessageVO vo);

    /**
     * 根据appkey和msgId查询推送历史
     * @param vo
     * @return
     */
    ResponseModel queryHistoryMsgId(PushMessageVO vo);

}
