package com.anji.sp.push.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * @Author: Kean
 * @Date: 2021/2/20
 * @Description:
 */
public class AJPushMessage {
    private ArrayList<String> deviceIds;//设备的唯一id
    private String title;//推送标题
    private String content;//推送内容
    private Map<String, String> extras;//传递的参数
    private Map<String, Object> androidConfig;//iOS配置项 json {"sound": "sound"}
    private Map<String, Object> iosConfig;//android配置项 json {"sound": "sound"}
    private String pushType;//1：透传 走极光 0：走厂商通道
    private String appKey;//appKey
    private String secretKey;//secretKey


    public AJPushMessage(ArrayList<String> deviceIds, String title, String content, Map<String, String> extras, Map<String, Object> androidConfig, Map<String, Object> iosConfig, String pushType, String appKey, String secretKey) {
        this.deviceIds = deviceIds;
        this.title = title;
        this.content = content;
        this.extras = extras;
        if (Objects.isNull(androidConfig)){
            HashMap<String, Object> androidConfigMap = new HashMap<>();
            androidConfigMap.put("sound", "");
            this.androidConfig = androidConfigMap;
        }else {
            this.androidConfig = androidConfig;
        }

        if (Objects.isNull(iosConfig)){
            HashMap<String, Object> iosConfigMap = new HashMap<>();
            iosConfigMap.put("sound", "default");
            this.iosConfig = iosConfigMap;
        }else {
            this.iosConfig = iosConfig;
        }
        this.pushType = pushType;
        this.appKey = appKey;
        this.secretKey = secretKey;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    public AJPushMessage() {
    }

    public ArrayList<String> getDeviceIds() {
        return deviceIds;
    }

    public void setDeviceIds(ArrayList<String> deviceIds) {
        this.deviceIds = deviceIds;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Map<String, String> getExtras() {
        return extras;
    }

    public void setExtras(Map<String, String> extras) {
        this.extras = extras;
    }

    public Map<String, Object> getAndroidConfig() {
        return androidConfig;
    }

    public void setAndroidConfig(Map<String, Object> androidConfig) {
        this.androidConfig = androidConfig;
    }

    public Map<String, Object> getIosConfig() {
        return iosConfig;
    }

    public void setIosConfig(Map<String, Object> iosConfig) {
        this.iosConfig = iosConfig;
    }

    public String getPushType() {
        return pushType;
    }

    public void setPushType(String pushType) {
        this.pushType = pushType;
    }

    public String getAppKey() {
        return appKey;
    }

    public void setAppKey(String appKey) {
        this.appKey = appKey;
    }

    @Override
    public String toString() {
        return "AJPushMessage{" +
                "deviceIds=" + deviceIds +
                ", title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", extras=" + extras +
                ", androidConfig=" + androidConfig +
                ", iosConfig=" + iosConfig +
                ", pushType='" + pushType + '\'' +
                ", appKey='" + appKey + '\'' +
                '}';
    }
}
